/// <reference types="cypress" />
// ***********************************************************
// This example plugins/index.js can be used to load plugins
//
// You can change the location of this file or turn off loading
// the plugins file with the 'pluginsFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/plugins-guide
// ***********************************************************
const {Properties} = require( "../config/Properties" );
const browserify = require('@cypress/browserify-preprocessor');
const cucumber = require('cypress-cucumber-preprocessor').default;

//require('cypress-xpath');
/**
 * @type {Cypress.PluginConfig}
 */
// eslint-disable-next-line no-unused-vars
module.exports = (on, config) => {
    /***
     * typescript
     */
    const options = browserify.defaultOptions;
    options.typescript = process.cwd()+"/node_modules/typescript";
    /***
     * cucumber json
     */
    on('file:preprocessor', cucumber(options));
    /***
     *  Dynamic Configuration
     */
    return Properties
        .newInstance(config)
        .reporter( config.env.Dreporter || "multi" )
        .setEnv( config.env.Denv || "prod" )
        .deviceSize( config.env.Ddevice || "desktop" )
        .getConfig();
}

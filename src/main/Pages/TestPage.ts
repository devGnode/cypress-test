
export class TestPage{

    public static readonly INSTANCE: TestPage = new TestPage();

    private static readonly url:string = "/ncr";

    // String handles
    public static readonly HOME_PAGE_ACCEPT_BTN:string = "//button//div[contains(text(),'accept')]";
    public static readonly SEARCH_INPUT:string         = "//input[@type='search']";

    constructor() {}

    public getUrl( ):string{return TestPage.url;}

    public static getInstance( ):TestPage{
        return TestPage.INSTANCE;
    }

}
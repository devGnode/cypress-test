import {EREPORTER_TYPE, EVIEW_TYPE, REPORTER_TYPE, VIEW_TYPE} from "./IProperties";


export class Properties {

    private static ANDROID_USERAGENT:string = "Mozilla/5.0 (Linux; Android 11; SM-A205U) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.91 Mobile Safari/537.36"
    private static IPHONE_USERAGENT:string  = "Mozilla/5.0 (iPhone; CPU iPhone OS 12_2 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148";

    private readonly config:any = null;

    constructor(config: any = null ) {
        this.config = config
    }

    public setEnv( env:string = "prod" ):Properties{
        try {
            this.config.baseUrl = require(process.cwd()+`/src/resources/env/${env}.env.conf.json`).baseUrl;
        }catch (e) {
            throw new Error(e);
        }
        return this;
    }

    public mochaJunitReporter():Properties{
        this.config.reporter = "mocha-junit-reporter";
        this.config.reporterOptions = {
            mochaFile:"target/junit/junit-report-[hash].xml",
            testsuitesTitle: true,
            jenkinsMod: true
        };
        return this;
    }

    public mochawesomeReporter( ):Properties{
        this.config.reporter = "mochawesome";
        this.config.reporterOptions = {
            html: true,
            json: true,
            reportDir:"target/mochawesome",
            reportFilename: "index"
        };
        return this;
    }

    public allureReporting( ):Properties{
        this.config.reporter = "mocha-allure-reporter";
        this.config.reporterOptions = {
            targetDir:"target/allure-results"
        };
        return this;
    }

    public multiReporters( ):Properties{
        this.config.reporter = "cypress-multi-reporters";
        this.config.reporterOptions = {
            configFile:"src/resources/reporter.conf.json"
        };
        return this;
    }

    public reporter( type:REPORTER_TYPE = EREPORTER_TYPE.MULTI ):Properties{
        switch (type) {
           case EREPORTER_TYPE.JUNIT: this.mochaJunitReporter(); break;
           case EREPORTER_TYPE.ALLURE: this.allureReporting(); break;
           case EREPORTER_TYPE.MOCHAWESOME: this.mochawesomeReporter(); break;
           case EREPORTER_TYPE.MULTI: this.multiReporters(); break;
           case EREPORTER_TYPE.NONE:
                this.config.reporter = "";
                this.config.reporterOptions = null;
        }
        return this;
    }

    private userAgent(type:VIEW_TYPE = EVIEW_TYPE.DESKTOP ):Properties {
        switch (type) {
            case EVIEW_TYPE.IPHONEX:
            case EVIEW_TYPE.IPAD:
              this.config.userAgent = Properties.IPHONE_USERAGENT;
            break;
            case EVIEW_TYPE.GALAXYS5:
                this.config.userAgent = Properties.ANDROID_USERAGENT;
            break;
            default:
        }
        return this;
    }

    public deviceSize( type:VIEW_TYPE = EVIEW_TYPE.DESKTOP ):Properties{
        let width:number = 1000,
            height:number = 600;

        switch (type) {
            case EVIEW_TYPE.DESKTOP:
                width = 1920;
                height= 1080;
             break;
            case EVIEW_TYPE.IPHONEX:
                width = 360;
                height= 640;
            break;
            case EVIEW_TYPE.IPAD:
                width = 768;
                height= 1024;
            break;
            case EVIEW_TYPE.GALAXYS5:
                width = 360;
                height= 640;
            break;
            default:
        }

       this.userAgent(type);
       this.config.viewportWidth = width;
       this.config.viewportHeight = height;

        return this;
    }

    public getConfig():any{ return this.config; }

    public static newInstance( config: any = null):Properties{
        return new Properties(config);
    }
}
export type REPORTER_TYPE = "allure" | "junit" | "mochawesome" | "multi" | "none"
export type VIEW_TYPE = "desktop" | "iphoneX" | "ipad" | "galaxyS5"

export enum EVIEW_TYPE{
    DESKTOP     = "desktop",
    IPHONEX     = "iphoneX",
    IPAD        = "ipad",
    GALAXYS5    = "galaxyS5"
}

export enum EREPORTER_TYPE{
    ALLURE          =  "allure",
    JUNIT           = "junit",
    MOCHAWESOME     = "mochawesome",
    MULTI           = "multi",
    NONE            = "none"
}
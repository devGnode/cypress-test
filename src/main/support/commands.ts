// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })
// @ts-ignore
//  Cypress.Commands.overwrite('visit', (originalFn, url, options) => {  })
require('cypress-xpath');

declare namespace Cypress {
    interface Chainable {
        /**
         * Custom command to select DOM element by data-cy attribute.
         * @example cy.dataCy('greeting')
         */
        clickOn(selector: string): any
    }
}
/****
 *
 */
Cypress.Commands.add('clickOn', (selector:string)=>{
    (/^(\/\/|\/).*/.test(selector) ? cy.xpath : cy.get ).call(cy, selector).click();
});

Cypress.Commands.overwrite('get',(originalFn: Function, selector: string,options: Object)=>{
    return /^(\/\/|\/).*/.test(selector) ?
        cy.xpath(selector,options) :
        originalFn(selector,options);
});

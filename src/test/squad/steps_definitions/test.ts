import { Given, When,Then } from "cypress-cucumber-preprocessor/steps";
import {TestPage} from "../../../main/Pages/TestPage"

Given("I open Google page", () => {
    cy.visit(TestPage.getInstance().getUrl())
});

When(/I click on accept button/,()=>{
    cy.clickOn(TestPage.HOME_PAGE_ACCEPT_BTN);
});

When(/I make a research/,()=>{
    cy.get(TestPage.SEARCH_INPUT).type("Hello World !");
});

Then(/I see \"([^\"]*)\" in the title/, (title) => {

});